package top.hmtools.wxmp.webpage.jsSdk;

/**
 * 签名算法方式
 * @author Hybomyth
 *
 */
public enum ESignType {

	MD5,
	
	SHA1
	;
}
