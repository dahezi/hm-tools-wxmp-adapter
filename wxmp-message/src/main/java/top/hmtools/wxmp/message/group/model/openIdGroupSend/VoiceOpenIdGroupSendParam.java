package top.hmtools.wxmp.message.group.model.openIdGroupSend;

/**
 * 语音：
 * @author HyboWork
 *
 */
public class VoiceOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private MediaId voice;

	public MediaId getVoice() {
		return voice;
	}

	public void setVoice(MediaId voice) {
		this.voice = voice;
	}

	@Override
	public String toString() {
		return "VoiceOpenIdGroupSendParam [voice=" + voice + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
