package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送菜单消息
 * FIXME TODO 当客服发送此目录消息给用户后，用户选择其中一项后，微信服务器会推送一条事件消息（xml数据格式）
 * @author HyboWork
 *
 */
public class MenuCustomerServiceMessage extends BaseSendMessageParam {

	private Msgmenu msgmenu;

	public Msgmenu getMsgmenu() {
		return msgmenu;
	}

	public void setMsgmenu(Msgmenu msgmenu) {
		this.msgmenu = msgmenu;
	}

	@Override
	public String toString() {
		return "MenuCustomerServiceMessage [msgmenu=" + msgmenu + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}


}
