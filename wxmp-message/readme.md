[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-message.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-message%22)
#### 前言
本组件对应实现微信公众平台“消息管理”章节相关api接口、事件推送，原接口文档地址：[消息管理](https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html)

#### 接口说明
- `top.hmtools.wxmp.message.customerService.apis.ICustomerServiceApi`  旧版客服消息
- `top.hmtools.wxmp.message.group.apis.IGroupMessageApi`  群发消息接口
- `top.hmtools.wxmp.message.template.apis.ITemplateApi`   模板消息接口

#### 事件消息类说明
1. 接收普通消息	`top.hmtools.wxmp.message.ordinary.model`
2. 接收事件推送   `top.hmtools.wxmp.message.eventPush.model`
3. 被动回复用户消息   `top.hmtools.wxmp.message.reply.model`
4. 消息加解密说明
5. 客服消息		`top.hmtools.wxmp.message.customerService`，
6. 群发接口和原创校验  `top.hmtools.wxmp.message.group`
7. 模板消息接口	`top.hmtools.wxmp.message.template`
8. 一次性订阅消息
9. 模板消息运营规范
10. 接口调用频次限制说明
11. 获取公众号的自动回复规则

#### 使用示例
0. 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-message</artifactId>
  <version>1.0.0</version>
</dependency>
```

1. 获取wxmpSession，参照 [wxmp-core/readme.md](../wxmp-core/readme.md)
```
//2 获取动态代理对象实例
ITemplateApi templateApi = wxmpSession.getMapper(ITemplateApi.class);

//3 获取所有模板信息
TemplateListResult result = templateApi.getAllPrivateTemplate();
```
更多示例参见：
- 统一的消息处理 [WxmpMessageTest.java](src/test/java/top/hmtools/wxmp/message/WxmpMessageTest.java)
- 接收普通消息 [OrdinaryMessageTestController.java](src/test/java/top/hmtools/wxmp/message/OrdinaryMessageTestController.java)
- 接收事件推送 [EventPushMessageTestController.java](src/test/java/top/hmtools/wxmp/message/EventPushMessageTestController.java)  TODO  还有一个需要验证eventKey的消息测试不通过
- 被动回复用户消息 [ReplyMessagesTest.java](src/test/java/top/hmtools/wxmp/message/ReplyMessagesTest.java)
- 客服消息  [ICustomerServiceApi.java](src/main/java/top/hmtools/wxmp/message/customerService/apis/ICustomerServiceApi.java) 仅`发送消息`，`客服输入状态`测试通过，其它接口微信已停用？？
- 群发接口和原创校验  [IGroupMessageApiTest.java](src/test/java/top/hmtools/wxmp/message/group/apis/IGroupMessageApiTest.java)  因使用测试公众号进行测试，部分接口无权限，应该是OK的。    
[GroupMessageSendEventTest.java](src/test/java/top/hmtools/wxmp/message/group/model/event/GroupMessageSendEventTest.java)  测试OK 
- 模板消息接口 [ITemplateApiTest.java](src/test/java/top/hmtools/wxmp/message/template/apis/ITemplateApiTest.java)  部分接口因使用测试公众号没有权限而没测通。
[TemplateMessageSendedEventTest.java](src/test/java/top/hmtools/wxmp/message/template/model/event/TemplateMessageSendedEventTest.java)  测试OK








