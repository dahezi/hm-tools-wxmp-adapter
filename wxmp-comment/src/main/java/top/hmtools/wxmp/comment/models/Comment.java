package top.hmtools.wxmp.comment.models;

/**
 * Auto-generated: 2019-08-21 22:16:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Comment {

	/**
	 * 是否精选评论，0为即非精选，1为true，即精选
	 */
	private int comment_type;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 评论时间
	 */
	private String create_time;
	private String openid;

	/**
	 * 作者回复
	 */
	private Reply reply;
	/**
	 * 用户评论id
	 */
	private String user_comment_id;

	public void setComment_type(int comment_type) {
		this.comment_type = comment_type;
	}

	public int getComment_type() {
		return comment_type;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getOpenid() {
		return openid;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}

	public Reply getReply() {
		return reply;
	}

	public void setUser_comment_id(String user_comment_id) {
		this.user_comment_id = user_comment_id;
	}

	public String getUser_comment_id() {
		return user_comment_id;
	}

	@Override
	public String toString() {
		return "Comment [comment_type=" + comment_type + ", content=" + content + ", create_time=" + create_time
				+ ", openid=" + openid + ", reply=" + reply + ", user_comment_id=" + user_comment_id + "]";
	}

}