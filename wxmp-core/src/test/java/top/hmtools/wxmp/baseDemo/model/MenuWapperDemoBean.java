package top.hmtools.wxmp.baseDemo.model;

/**
 * 自定义菜单目录包装类
 * @author HyboWork
 *
 */
public class MenuWapperDemoBean {

	/**
	 * 缺省的自定义菜单
	 */
	private MenuDemoBean menu;
	

	public MenuDemoBean getMenu() {
		return menu;
	}

	public void setMenu(MenuDemoBean menu) {
		this.menu = menu;
	}

	@Override
	public String toString() {
		return "MenuWapperBean [menu=" + menu  + "]";
	}

	
	
}
