package top.hmtools.wxmp.menu.models.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单信息
 * @author Hybomyth
 *
 */
public class SelfmenuInfoBean {
	
	private int maxCount = 3;

	/**
	 * 菜单按钮
	 */
	private List<SelfmenuInfoButtonBean> button;

	/**
	 * 菜单按钮
	 * @return
	 */
	public List<SelfmenuInfoButtonBean> getButton() {
		return button;
	}

	/**
	 * 菜单按钮
	 * @param button
	 */
	public void setButton(List<SelfmenuInfoButtonBean> button) {
		if(button !=null && button.size()>maxCount){
			this.button = new ArrayList<>();
			this.button.addAll(button.subList(0, maxCount));
		}else{
			this.button = button;
		}
	}
	
	/**
	 * 添加菜单按钮，最多添加3个，超出的会被忽略掉
	 * @param buttonBeans
	 * @return
	 */
	public synchronized SelfmenuInfoBean addButtion(SelfmenuInfoButtonBean... buttonBeans){
		if(this.button == null){
			this.button = new ArrayList<>();
		}
		
		if(buttonBeans == null || buttonBeans.length<1){
			return this;
		}
		
		if(this.button.size()<maxCount){
			for(SelfmenuInfoButtonBean item:buttonBeans	){
				if(this.button.size()<maxCount){
					this.button.add(item);
				}else{
					return this;
				}
			}
		}
		
		return this;
	}
}
