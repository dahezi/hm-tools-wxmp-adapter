package top.hmtools.wxmp.user.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.RemarkParam;

@WxmpMapper
public interface IRemarkApi {

	/**
	 * 设置用户备注名
	 * @param remarkParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/user/info/updateremark")
	public ErrcodeBean updateRemark(RemarkParam remarkParam);
}
